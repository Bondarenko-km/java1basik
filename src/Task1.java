import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        int radius;
        double volume;
        Scanner input = new Scanner(System.in);
        System.out.print("Введите радиус: ");
        radius = input.nextInt();
        volume = 4.0/3*Math.PI*Math.pow(radius,3);
        System.out.print("Объем шара при радиусе " + radius
                + " равен " + volume);

    }
}
