import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        final double MILE = 1.60934;

        Scanner input = new Scanner(System.in);
        System.out.print("Введите кол-во киллометров: ");
        int km = input.nextInt();
        System.out.println("В " + km + " киллометрах содержится "
                + km/MILE + " миль");
    }
}
