import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        long totalMinutes, currentMinute, totalHours, currentHour;

        Scanner input = new Scanner(System.in);
        System.out.print("Введите секунды ");
        int count = input.nextInt();

        totalMinutes = count/60;
        currentMinute = totalMinutes % 60;
        totalHours = totalMinutes / 60;
        currentHour = totalHours % 24;

        System.out.println("Текущее время равно " + currentHour + " часов "
                + currentMinute + " минут");
    }
}