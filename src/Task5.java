import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        final double DUIM = 2.54;

        Scanner input = new Scanner(System.in);
        System.out.print("Введите кол-во дюймов: ");
        int duim = input.nextInt();
        System.out.println("В " + duim + " дюймах содержится "
                + duim * DUIM + " сантиметров");
    }
}
