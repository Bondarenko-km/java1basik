import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите двузначное число: ");
        int n = input.nextInt();
        System.out.println("Число, полученное перестановкой цифр в исходном числе " + n%10 + n/10);


    }
}
