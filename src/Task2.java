import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int a;
        int b;
        Scanner input = new Scanner(System.in);
        System.out.print("Введите число а: ");
        a = input.nextInt();
        System.out.print("Введите число b: ");
        b = input.nextInt();
        double quadratic = Math.sqrt((Math.pow (a,2) + Math.pow (b,2))/2);
        System.out.print("Среднее квадратическое при введенных данных a и b равно " + quadratic);

    }
}