import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите бюджет мероприятия: ");
        int n = input.nextInt();
        System.out.print("Введите бюджет на одного гостя: ");
        int k = input.nextInt();
        System.out.println("На данное мероприятие можно пригласить " +
                n/k + " гостей");
    }
}

